WITH
    --Obtiene el listado de clientes por los filtros de tipo [TI] y ubicación [UG] en caso de ser nulos los obtiene todos
    clients AS (SELECT * FROM client c WHERE 1 = 1 AND c.type = 1), --OR / AND c.location = :location
    --Obtiene la suma del balance de todas la cuentas por cliente
    sum_balance AS (SELECT a.client_id, sum(a.balance) balance FROM account a GROUP BY a.client_id),
    --Ejecuta un join entre clients y sum_balance y filtra por el rango de sus balances [RI] - [RG] en caso de que no sean nulos
    order_clients AS (SELECT * FROM clients JOIN sum_balance
    WHERE clients.id = sum_balance.client_id
    --                       AND balance > :startBalance
    --                       AND balance < :endBalance
    ),
    --Agrupa los clientes y obtiene solo uno por empresa y genero
    distinct_company AS (SELECT DISTINCT FIRST_VALUE(id) OVER (PARTITION BY company, male ORDER BY balance DESC) id FROM order_clients),
    --Cuenta la cantidad de clientes por genero de la tabla distinct_company
    count_genders AS (SELECT oc.id, male, company, balance, count(*) OVER (PARTITION BY male) total_gender FROM order_clients oc INNER JOIN distinct_company dc on oc.id = dc.id),
    --Obtiene la cantidad minima de clientes en un genero para los filtros ya dados
    condition1 AS (SELECT id, male, company, balance, total_gender, FIRST_VALUE(total_gender) OVER (ORDER BY total_gender, balance DESC) min_gender_value FROM count_genders),
    --Obtiene los clientes del genero que menos personas tiene y retorna un maximo de 4, Ademas hace un union con todos los clientes del otro genero y obtiene su numero de fila
    condition2 AS ((SELECT *, 0 rowNum FROM condition1 where total_gender <= min_gender_value LIMIT 4) UNION
                   (SELECT *, (row_number() OVER ()) rowNum FROM condition1 WHERE total_gender > min_gender_value)),
    --Dado el numero de fila de condition2 (rowNum) obtiene los clientes restantes para el genero con mas coincidencias y limita el resultado a maximo 8 clientes
    final_Condition AS (SELECT *, count(*) OVER (PARTITION BY male order by balance DESC) FROM condition2 WHERE rowNum <= min_gender_value LIMIT 8)
    --Ejecuta el union por ID con la tabla client para mapear el resultado en la entidad ClientEntity y los ordena por codigo
    SELECT c.* FROM client c INNER JOIN final_Condition fc ON fc.id = c.id ORDER BY fc.balance DESC, c.code;