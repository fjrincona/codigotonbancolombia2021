# Reto Programación General (Bancolombia) #

Un importante banco desea ofrecer una cena a grupos selectos de clientes. El banco definirá 6 grupos de clientes según ciertos criterios, los cuales serán invitados y cada grupo contará con una mesa. Las mesas cuentan con 8 puestos y en caso de que un grupo tenga menos de 4 clientes, la mesa no sería implementada.

Se requiere construir un programa que reciba los criterios para formar cada grupo y que según esto determine los clientes a ser invitados de acuerdo a los criterios indicados en la especificación. La salida del programa debe entregar los códigos de cliente asignados a cada mesa y es lo que debe pegar en esta sección.

Descargue los archivos con la estructura de datos, la especificación y el archivo de entrada a utilizar.

### Contenido ###

* [Requerimiento y recursos](https://bitbucket.org/fjrincona/codigotonbancolombia2021/src/master/resources/)
* [Solucion propuesta](https://bitbucket.org/fjrincona/codigotonbancolombia2021/src/master/SpringBootChallenge/)

### Licencia ###

Por la presente se otorga permiso, sin cargo, a cualquier persona que obtenga una copia de este software. No obstante los recursos que pertenecen al requerimiento son propiedad de Bancolombia.

@Autor: Dagondeveloper :)