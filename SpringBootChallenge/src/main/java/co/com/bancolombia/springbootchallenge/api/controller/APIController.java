package co.com.bancolombia.springbootchallenge.api.controller;

import co.com.bancolombia.springbootchallenge.api.service.guest.GuestServices;
import co.com.bancolombia.springbootchallenge.model.dto.guest.GuestRequestDTO;
import co.com.bancolombia.springbootchallenge.model.dto.guest.GuestResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api")
public class APIController {

    private final GuestServices guestServices;

    @SuppressWarnings("SameReturnValue")
    @GetMapping
    public String index() {
        return "index";
    }

    /**
     * Ejecuta el servicio encargado de determinar los clientes invitados por el banco, de acuerdo a los criterios
     * filtro que se obtienen del documento .txt recibido en el request
     *
     * @param request informacion de la peticion en formato JSON
     */
    @PostMapping(value = "/customers/dinner", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuestResponseDTO> getGuests(@RequestBody GuestRequestDTO request) {
        return ResponseEntity.ok().body(guestServices.getGuests(request));
    }

}
