package co.com.bancolombia.springbootchallenge.api.service.filter;

import co.com.bancolombia.springbootchallenge.model.dto.filter.FilterDTO;

import java.util.List;

public interface FilterServices {

    /**
     * Dado un listado de mesas como cadena, itera y setea todos los valores filtro a usar para cada una de las mesas
     *
     * @param desks Listado de mesas
     * @return El objeto FilterDTO con la informacion manipulable desde JAVA
     */
    List<FilterDTO> getFilters(List<String> desks);

    /**
     * Almacena todos los valores obtenidos por mesa en un objeto de JAVA manipulable
     *
     * @param desk         Mesa
     * @param customer     Tipo de cliente
     * @param location     Ubicacion geografica
     * @param startBalance Balance inicial
     * @param endBalance   Balance final
     * @return Una nueva instancia del objeto FilterDTO
     */
    FilterDTO newFilter(String desk, String customer, String location, String startBalance, String endBalance);

}
