package co.com.bancolombia.springbootchallenge.model.dto.guest;

import co.com.bancolombia.springbootchallenge.model.dto.log.LogResponseDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GuestResponseDTO {

    private String response;
    private LogResponseDTO log;

}
