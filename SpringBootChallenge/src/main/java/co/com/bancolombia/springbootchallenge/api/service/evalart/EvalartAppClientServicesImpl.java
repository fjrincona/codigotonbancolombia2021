package co.com.bancolombia.springbootchallenge.api.service.evalart;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class EvalartAppClientServicesImpl implements EvalartAppClientServices {

    private final RestTemplate restTemplate;

    @Override
    public String decrypt(String code) {
        Map<String, String> params = new HashMap<>();
        params.put("code", code);
        final String URL_MAIN = "https://test.evalartapp.com/extapiquest/code_decrypt/{code}";
        return Objects.requireNonNull(restTemplate.getForObject(URL_MAIN, String.class, params)).replace("\"","");
    }

}
