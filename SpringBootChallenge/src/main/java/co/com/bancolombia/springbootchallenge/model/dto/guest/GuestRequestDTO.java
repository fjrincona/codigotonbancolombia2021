package co.com.bancolombia.springbootchallenge.model.dto.guest;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GuestRequestDTO {

    private String fileNamePath;

}
