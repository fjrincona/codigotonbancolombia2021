package co.com.bancolombia.springbootchallenge.api.service.guest;

import co.com.bancolombia.springbootchallenge.model.dto.guest.GuestRequestDTO;
import co.com.bancolombia.springbootchallenge.model.dto.guest.GuestResponseDTO;

public interface GuestServices {

    /**
     * Ejecuta el programa principal encargado de obtener los invitados
     *
     * @param request Información de la solicitud
     * @return La entidad GuestResponseDTO, el atributo Response corresponde a la respuesta solicitada por para cumplir
     * el reto, la respuesta también se imprime en un logger para obtenerla sin los caracteres de escape
     */
    GuestResponseDTO getGuests(GuestRequestDTO request);

}
