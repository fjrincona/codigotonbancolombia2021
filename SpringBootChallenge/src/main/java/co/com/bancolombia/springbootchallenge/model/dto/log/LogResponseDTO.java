package co.com.bancolombia.springbootchallenge.model.dto.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class LogResponseDTO {

    private LogType logType;
    private String logMessage;

}
