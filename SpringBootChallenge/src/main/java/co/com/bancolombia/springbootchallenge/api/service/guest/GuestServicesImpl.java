package co.com.bancolombia.springbootchallenge.api.service.guest;

import co.com.bancolombia.springbootchallenge.api.repository.client.ClientRepo;
import co.com.bancolombia.springbootchallenge.api.service.evalart.EvalartAppClientServices;
import co.com.bancolombia.springbootchallenge.api.service.filter.FilterServices;
import co.com.bancolombia.springbootchallenge.api.util.file.FileUtils;
import co.com.bancolombia.springbootchallenge.model.dto.filter.FilterDTO;
import co.com.bancolombia.springbootchallenge.model.dto.guest.GuestRequestDTO;
import co.com.bancolombia.springbootchallenge.model.dto.guest.GuestResponseDTO;
import co.com.bancolombia.springbootchallenge.model.dto.log.LogResponseDTO;
import co.com.bancolombia.springbootchallenge.model.entity.ClientEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

import static co.com.bancolombia.springbootchallenge.model.dto.log.LogType.ERROR;
import static co.com.bancolombia.springbootchallenge.model.dto.log.LogType.SUCCESS;

@Service
@Slf4j
@RequiredArgsConstructor
public class GuestServicesImpl implements GuestServices {

    private final FilterServices filterServices;
    private final EvalartAppClientServices evalartServices;
    private final ClientRepo clientRepo;

    @Override
    public GuestResponseDTO getGuests(GuestRequestDTO request) {
        GuestResponseDTO response = new GuestResponseDTO();
        LogResponseDTO logResponse = new LogResponseDTO();
        try {
            List<String> desks = FileUtils.readFileByLine(request.getFileNamePath(), "<");
            List<FilterDTO> filters = filterServices.getFilters(desks);
            response.setResponse(getResponse(filters));
            logResponse.setLogType(SUCCESS);
            logResponse.setLogMessage("Solicitud ejecutada correctamente");
        } catch (Exception e) {
            logResponse.setLogType(ERROR);
            logResponse.setLogMessage("Error causado por: " + e.getMessage());
            e.printStackTrace();
        }
        response.setLog(logResponse);
        return response;
    }

    private String getResponse(List<FilterDTO> filters) {
        StringBuilder sb = new StringBuilder();
        final String BREAK_LINE = System.lineSeparator();
        final String COMMA = ",";
        filters.forEach(
                f -> {
                    sb.append("<").append(f.getDeskName()).append(">").append(BREAK_LINE);
                    List<ClientEntity> clients = clientRepo.getClientsByFilters(f);
                    if (clients.size() < 4) {
                        sb.append("CANCELADA");
                    } else {
                        clients.forEach(
                                c -> sb.append(c.getEncrypt() == 1 ?
                                        evalartServices.decrypt(c.getCode()) : c.getCode()).append(COMMA));
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    sb.append(BREAK_LINE);
                }
        );
        String responseFinal = sb.deleteCharAt(sb.length() - 1).toString();
        //Se imprime en el log para obtener la cadena sin caracteres de escape
        log.info(responseFinal);
        return responseFinal;
    }

}
