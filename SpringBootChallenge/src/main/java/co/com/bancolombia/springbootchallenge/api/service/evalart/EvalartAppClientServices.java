package co.com.bancolombia.springbootchallenge.api.service.evalart;

public interface EvalartAppClientServices {

    /**
     * Ejecuta un servicio de EvalartApp que desencripta el codigo entrante
     *
     * @param code Codigo encriptado
     * @return Código desencriptado
     */
    String decrypt(String code);

}
