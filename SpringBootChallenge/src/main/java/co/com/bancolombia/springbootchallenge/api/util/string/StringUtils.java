package co.com.bancolombia.springbootchallenge.api.util.string;

/**
 * Utilidad para el manejo de cadenas de texto en la aplicacion
 *
 * @author dagon
 */
public class StringUtils {

    public static boolean isNullOrEmpty(String text) {
        return text != null && !text.isEmpty() && !text.trim().isEmpty();
    }

    public static String substring(String text, String character) {
        return isNullOrEmpty(text) ? text.substring(0, text.indexOf(character)) : null;
    }

    public static String split(String text, String delimiter) {
        return isNullOrEmpty(text) ? text.split(delimiter)[1] : null;
    }

}
