package co.com.bancolombia.springbootchallenge.model.dto.log;

public enum LogType {
    SUCCESS, WARNING, ERROR
}
