package co.com.bancolombia.springbootchallenge.api.repository.client;

import co.com.bancolombia.springbootchallenge.model.dto.filter.FilterDTO;
import co.com.bancolombia.springbootchallenge.model.entity.ClientEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@Slf4j
public class ClientRepo {

    private final EntityManager entityManager;

    public ClientRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<ClientEntity> getClientsByFilters(FilterDTO filter) {
        List<ClientEntity> list;
        try {
            //Obtiene el listado de clientes por los filtros de tipo [TI] y ubicación [UG] en caso de ser nulos los obtiene todos
            String customerType = filter.getCustomerType() != null ? " AND c.type = :type " : "";
            String location = filter.getLocation() != null ? " AND c.location = :location " : "";
            String clientQuery = " clients AS (SELECT * FROM client c WHERE 1 = 1 " + customerType + location + "), ";

            //Obtiene la suma del balance de todas la cuentas por cliente
            String sumBalanceQuery = " sum_balance AS (SELECT a.client_id, sum(a.balance) balance FROM account a GROUP BY a.client_id), ";

            //Ejecuta un join entre clients y sum_balance y filtra por el rango de sus balances [RI] - [RG] en caso de que no sean nulos
            String startBalance = filter.getStartBalance() != null ? " AND balance > :startBalance " : "";
            String endBalance = filter.getEndBalance() != null ? " AND balance < :endBalance " : "";
            String orderClients = " order_clients AS (SELECT * FROM clients JOIN sum_balance " +
                    " WHERE clients.id = sum_balance.client_id " + startBalance + endBalance + "), ";

            //Agrupa los clientes y obtiene solo uno por empresa y género
            String distinctCompany = " distinct_company AS (SELECT DISTINCT FIRST_VALUE(id) OVER (PARTITION BY company, male ORDER BY balance DESC) id FROM order_clients), ";

            //Cuenta la cantidad de clientes por género de la tabla distinct_company
            String countGenders = " count_genders AS (SELECT oc.id, male, company, balance, count(*) OVER (PARTITION BY male) total_gender FROM order_clients oc INNER JOIN distinct_company dc on oc.id = dc.id), ";

            //Obtiene la cantidad minima de clientes en un género para los filtros ya dados
            String condition1 = " condition1 AS (SELECT id, male, company, balance, total_gender, FIRST_VALUE(total_gender) OVER (ORDER BY total_gender, balance DESC) min_gender_value FROM count_genders), ";

            //Obtiene los clientes del género que menos personas tiene y retorna un maximo de 4, Ademas hace un union con todos los clientes del otro genero y obtiene su numero de fila
            String condition2 = " condition2 AS ((SELECT *, 0 rowNum FROM condition1 where total_gender <= min_gender_value LIMIT 4) UNION " +
                    " (SELECT *, (row_number() OVER ()) rowNum FROM condition1 WHERE total_gender > min_gender_value)), ";

            //Dado el número de fila de condition2 (rowNum) obtiene los clientes restantes para el género con más coincidencias y limita el resultado a maximo 8 clientes
            String finalCondition = " final_Condition AS (SELECT *, count(*) OVER (PARTITION BY male order by balance DESC) FROM condition2 WHERE rowNum <= min_gender_value LIMIT 8) ";

            //Ejecuta el union por ID con la tabla client para mapear el resultado en la entidad ClientEntity y los ordena por codigo
            String statementClient = " SELECT c.* From client c INNER JOIN final_Condition fc ON fc.id = c.id ORDER BY fc.balance DESC, c.code ";

            //Query principal que será ejecutada en la base de datos
            String statement = "WITH " +
                    clientQuery +
                    sumBalanceQuery +
                    orderClients +
                    distinctCompany +
                    countGenders +
                    condition1 +
                    condition2 +
                    finalCondition +
                    statementClient ;

            log.info(statement);

            Query query = entityManager.createNativeQuery(statement, ClientEntity.class);
            if (filter.getCustomerType() != null) {
                query.setParameter("type", Integer.valueOf(filter.getCustomerType()));
            }
            if (filter.getLocation() != null) {
                query.setParameter("location", Integer.valueOf(filter.getLocation()));
            }
            if (filter.getStartBalance() != null) {
                query.setParameter("startBalance", Integer.valueOf(filter.getStartBalance()));
            }
            if (filter.getEndBalance() != null) {
                query.setParameter("endBalance", Integer.valueOf(filter.getEndBalance()));
            }
            list = query.getResultList();
        } catch (Exception e) {
            return null;
        }
        return list;
    }

}
