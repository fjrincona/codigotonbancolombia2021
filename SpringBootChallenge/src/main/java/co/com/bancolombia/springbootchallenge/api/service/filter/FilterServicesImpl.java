package co.com.bancolombia.springbootchallenge.api.service.filter;

import co.com.bancolombia.springbootchallenge.api.util.file.FileUtils;
import co.com.bancolombia.springbootchallenge.api.util.string.StringUtils;
import co.com.bancolombia.springbootchallenge.model.dto.filter.FilterDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FilterServicesImpl implements FilterServices{

    @Override
    public List<FilterDTO> getFilters(List<String> desks) {
        List<FilterDTO> filters = new ArrayList<>();
        desks.forEach(d -> {
            List<String> deskByLine = FileUtils.readStringByLine(d);
            String deskName = deskByLine.stream().filter(it -> it.endsWith(">")).findFirst().orElse(null);
            String customerType = deskByLine.stream().filter(it -> it.startsWith("TC")).findFirst().orElse(null);
            String location = deskByLine.stream().filter(it -> it.startsWith("UG")).findFirst().orElse(null);
            String startBalance = deskByLine.stream().filter(it -> it.startsWith("RI")).findFirst().orElse(null);
            String endBalance = deskByLine.stream().filter(it -> it.startsWith("RF")).findFirst().orElse(null);
            filters.add(newFilter(deskName, customerType, location, startBalance, endBalance));
        });
        return filters;
    }

    @Override
    public FilterDTO newFilter(String desk, String customer, String location, String startBalance, String endBalance) {
        FilterDTO filter = new FilterDTO();
        filter.setDeskName(StringUtils.substring(desk, ">"));
        filter.setCustomerType(StringUtils.split(customer, ":"));
        filter.setLocation(StringUtils.split(location, ":"));
        filter.setStartBalance(StringUtils.split(startBalance, ":"));
        filter.setEndBalance(StringUtils.split(endBalance, ":"));
        return filter;
    }
}
