package co.com.bancolombia.springbootchallenge.api.util.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Utilidades para la lectura y manipulación de archivos
 *
 * @author dagon
 */
public class FileUtils {

    public static List<String> readFileByLine(String fileName, String delimiter) throws IOException {
        List<String> output = new ArrayList<>();
        Scanner scanner = new Scanner(new File(fileName)).useDelimiter(delimiter);
        while (scanner.hasNext()) {
            output.add(scanner.next());
        }
        return output;
    }

    public static List<String> readStringByLine(String text) {
        List<String> output = new ArrayList<>();
        Scanner scanner = new Scanner(text);
        while (scanner.hasNextLine()) {
            output.add(scanner.nextLine());
        }
        return output;
    }

}
