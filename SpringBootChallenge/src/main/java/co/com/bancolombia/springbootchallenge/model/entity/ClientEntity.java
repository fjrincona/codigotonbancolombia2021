package co.com.bancolombia.springbootchallenge.model.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "client", schema = "evalart_reto")
public class ClientEntity {
    private int id;
    private String code;
    private int male;
    private int type;
    private String location;
    private String company;
    private int encrypt;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "male")
    public int getMale() {
        return male;
    }

    public void setMale(int male) {
        this.male = male;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "company")
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Basic
    @Column(name = "encrypt")
    public int getEncrypt() {
        return encrypt;
    }

    public void setEncrypt(int encrypt) {
        this.encrypt = encrypt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity that = (ClientEntity) o;
        return id == that.id && male == that.male && type == that.type && encrypt == that.encrypt && Objects.equals(code, that.code) && Objects.equals(location, that.location) && Objects.equals(company, that.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, male, type, location, company, encrypt);
    }
}
