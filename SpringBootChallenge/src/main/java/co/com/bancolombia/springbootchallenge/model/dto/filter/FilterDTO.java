package co.com.bancolombia.springbootchallenge.model.dto.filter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FilterDTO {

    private String deskName;
    private String customerType;
    private String location;
    private String startBalance;
    private String endBalance;

}
